import { icons } from './../../../_interfaces/devicon';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Technology } from 'src/app/_interfaces/technology';

@Component({
  selector: 'app-tech-card',
  templateUrl: './tech-card.component.html',
  styleUrls: ['./tech-card.component.scss'],
})
export class TechCardComponent implements OnInit {
  @Input() tech: Technology;
  @Output() techChange: EventEmitter<Technology> = new EventEmitter<Technology>();
  @Output() deleteChange: EventEmitter<Technology> = new EventEmitter<Technology>();
  @Output() favouriteChange: EventEmitter<Technology> = new EventEmitter<Technology>();
  @Output() positionChange: EventEmitter<Technology> = new EventEmitter<Technology>();

  value = 20;

  constructor() {}

  ngOnInit() {
    this.value = this.tech.level * 20;
  }

  getIcon(inputName: string): string {
    if (icons[inputName.toLowerCase()]) return icons[inputName.toLowerCase()].iconName;
  }

  get status() {
    return 'success';
  }

  get canIncrease(): boolean {
    return this.value < 100;
  }

  get canDecrease(): boolean {
    return this.value > 20;
  }

  changePosition() {
    this.positionChange.emit(this.tech);
  }

  toggleFavourite() {
    this.tech.isFavourite = !this.tech.isFavourite;
    this.favouriteChange.emit(this.tech);
  }

  decreaseValue() {
    if (this.value > 0) {
      this.value -= 20;
      this.tech.level -= 1;
      this.techChange.emit(this.tech);
    }
  }

  increaseValue() {
    if (this.value < 100) {
      this.value += 20;
      this.tech.level += 1;
      this.techChange.emit(this.tech);
    }
  }

  deleteTech() {
    this.deleteChange.emit(this.tech);
  }
}
