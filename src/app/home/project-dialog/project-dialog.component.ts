import { ImageDialogComponent } from './image-dialog/image-dialog.component';
import { AuthService } from './../../_services/auth.service';
import { IUser } from './../../_interfaces/user';
import { Image } from './../../_interfaces/image';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FirebaseService } from './../../_services/firebase.service';
import { Profile } from './../../_interfaces/profile';
import { Project } from './../../_interfaces/project';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { icons } from '../../_interfaces/devicon';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';

const maxImgSize: number = 1100000;

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.scss'],
})
export class ProjectDialogComponent implements OnInit {
  @Input() project: Project;
  @Input() profile: Profile;
  @Input() isOwner: boolean;
  @ViewChild('fileInputDropzone') fileInputDropzone: ElementRef;
  @ViewChild('fileInputPreview') fileInputPreview: ElementRef;
  @ViewChild('autoCompleteInputProject', { read: MatAutocompleteTrigger }) autoComplete: MatAutocompleteTrigger; //used for scroll event to close it

  editActivated: boolean = false;
  firstForm: FormGroup;
  temporaryProjectName: string;
  files: File[] = [];

  newImages: Image[] = [];
  tempTechnologies: string[];
  previewImage: Image;
  percentagePreviewImage: Observable<number>;
  pendingFiles: number = 0; //files that have been added but did not finish uploading yet

  iframeStartedLoading: boolean = false;
  iframeLoaded: boolean = false;
  markdownPlaceholder: string = `# h1 Heading
###### h6 Heading

* List item1
* List item2

\`\`\`javascript
var x = "code highlighting";
\`\`\`
  `;

  techCtrl = new FormControl();
  filteredTechs: Observable<string[]>;
  previewTechs: string[] = [];

  constructor(
    protected ref: NbDialogRef<ProjectDialogComponent>,
    private firebaseService: FirebaseService,
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    public authService: AuthService,
    private dialogService: NbDialogService
  ) {
    //if autocomplete input is empty, don't display techs
    this.techCtrl.valueChanges.subscribe((tech) => {
      if (this.techCtrl.value && this.techCtrl.value.length > 0) {
        this.setIconArray();
      } else {
        this.previewTechs = [];
      }
    });
    //filter techs on change
    this.filteredTechs = this.techCtrl.valueChanges.pipe(
      map((tech) => (tech ? this._filterTechs(tech) : this.previewTechs.slice()))
    );
  }

  ngOnInit() {
    //trigger scroll event (hide autocomplete)
    window.addEventListener('scroll', this.scrollEvent, true);

    this.ref.onBackdropClick.subscribe(() => {
      this.closeDialog();
    });

    this.firstForm = this.fb.group({
      //temporaryProjectName binding via ngModel (outside of form)
      summary: ['', [Validators.required]],
      overview: [],
      description: [],
      challenges: [],
      requirements: [],
      company: [],
      publicUrl: [],
      githubRepository: [],
      previewColor: [],
      repl: [],
    });
    this.setValues();
  }

  private _filterTechs(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.previewTechs.filter((tech) => tech.toLowerCase().indexOf(filterValue) === 0);
  }
  setIconArray() {
    this.previewTechs = [];
    for (let key in icons) {
      this.previewTechs.push(icons[key].displayName);
    }
  }
  getIcon(inputName: string): string {
    if (icons[inputName.toLowerCase()]) return icons[inputName.toLowerCase()].iconName;
  }

  //if user scrolls inside the dialog, the autocomplete field will close
  scrollEvent = (event: any): void => {
    if (this.autoComplete && this.autoComplete.panelOpen) this.autoComplete.closePanel();
  };

  setValues() {
    this.temporaryProjectName = this.project.name;
    this.firstForm.controls.overview.setValue(this.project.overview);
    this.firstForm.controls.company.setValue(this.project.company);
    this.firstForm.controls.publicUrl.setValue(this.project.publicUrl);
    this.firstForm.controls.githubRepository.setValue(this.project.githubRepository);
    this.firstForm.controls.summary.setValue(this.project.summary);
    this.firstForm.controls.repl.setValue(this.project.repl);

    if (this.project.previewColor == 'white') this.firstForm.controls.previewColor.setValue('1');
    else this.firstForm.controls.previewColor.setValue('2');

    if (this.project.previewImage) this.previewImage = this.project.previewImage;

    this.tempTechnologies = JSON.parse(JSON.stringify(this.project.technologies));
    if (!this.tempTechnologies) this.tempTechnologies = [];
    this.files = [];
    this.newImages = [];

    //repl.it
    this.iframeStartedLoading = false;
    this.iframeLoaded = false;
  }

  closeDialog() {
    //gets called if user closes dialog
    this.cancelEdit();
    if (this.pendingFiles === 0) this.ref.close();
  }

  cancelEdit() {
    //gets called if user closes window and was still in edit mode
    this.editActivated = false;
    this.setValues();
  }

  edit() {
    //todo: check for right name, validator alternative
    if (
      this.editActivated &&
      this.temporaryProjectName != '' &&
      this.tempTechnologies.length > 0 &&
      this.firstForm.valid &&
      this.pendingFiles === 0
    ) {
      //update project data
      this.project.name = this.temporaryProjectName;
      this.project.technologies = JSON.parse(JSON.stringify(this.tempTechnologies));

      if (this.firstForm.controls.previewColor.value == '1') this.project.previewColor = 'white';
      else this.project.previewColor = '#222b45';

      this.project.summary = this.firstForm.controls.summary.value;
      this.project.overview = this.firstForm.controls.overview.value;
      this.project.company = this.firstForm.controls.company.value;
      this.project.publicUrl = this.firstForm.controls.publicUrl.value;
      this.project.githubRepository = this.firstForm.controls.githubRepository.value;

      if (this.firstForm.controls.repl.value) {
        if (this.firstForm.value.repl.substring(0, 15) === 'https://repl.it') {
          if (
            this.firstForm.controls.repl.value.substring(
              this.firstForm.controls.repl.value.length - 10,
              this.firstForm.controls.repl.value.length
            ) === '?lite=true'
          )
            this.project.repl = this.firstForm.controls.repl.value;
          else {
            this.project.repl = this.firstForm.controls.repl.value + '?lite=true'; //todo + skip all after ?
            this.firstForm.controls.repl.setValue(this.firstForm.controls.repl.value + '?lite=true');
          }
        } else {
          this.firstForm.controls.repl.setValue(null);
          this.project.repl = null;
        }
      } else {
        this.firstForm.controls.repl.setValue(null);
        this.project.repl = null;
      }

      this.tempTechnologies = [];
      this.files = [];

      this.firebaseService.updateProfile(this.profile);
      this.editActivated = !this.editActivated;
    } else if (this.editActivated) {
      window.alert('Please fill out all required input fields!');
    } else {
      //user activates editing mode
      this.editActivated = !this.editActivated;
      this.tempTechnologies = JSON.parse(JSON.stringify(this.project.technologies));
    }
  }

  removeImage(image: Image) {
    //gets called if user clicks remove image button
    for (let index = 0; index < this.project.images.length; index++) {
      if (this.project.images[index] == image) {
        this.storage.storage.refFromURL(this.project.images[index].url).delete(); //delete from storage
        this.project.images.splice(index, 1);
        break;
      }
    }
    this.firebaseService.updateProfile(this.profile);
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      if (files.length < 6 && this.project.images.length < 5) {
        let filesplit = files.item(i).name.split('.');
        let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
        if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
          let imgSize: number = files.item(i).size;
          if (imgSize <= maxImgSize) {
            this.files.push(files.item(i));
            this.pendingFiles++;
          } else
            window.alert(
              'Max file size currently is 1MB (automatic image compression will be added in a future update)'
            );
        } else window.alert('Only images (jpg, png, gif) allowed!');
      } else window.alert('Only 6 images allowed!');
    }
  }
  addDropzone() {
    // gets called when user clicks the dropzone; if user selects file
    // it calls the file input change method onDropClick()
    this.fileInputDropzone.nativeElement.click();
  }

  onDropClick(event) {
    if (event.target.files.length > 0) {
      if (this.project.images.length < 6) {
        let filesplit = event.target.files[0].name.split('.');
        let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
        if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
          let imgSize: number = event.target.files[0].size;
          if (imgSize <= maxImgSize) {
            this.files.push(event.target.files[0]);
            this.pendingFiles++;
          } else
            window.alert(
              'Max file size currently is 1MB (automatic image compression will be added in a future update)'
            );
        } else window.alert('Only images (jpg, png, gif) allowed!');
      } else window.alert('Only 6 images allowed!');
    }
  }

  newImageUploaded(image: Image) {
    //gets called if upload-task-component finished uploading the image to stoarge
    if (!this.project.images) this.project.images = [];

    this.pendingFiles--;
    this.project.images.push(image); //push to project
    this.firebaseService.updateProfile(this.profile); //update profile
  }

  deleteProject() {
    for (let index = 0; index < this.profile.projects.length; index++) {
      if (this.profile.projects[index] === this.project) {
        this.profile.projects.splice(index, 1);
        break;
      }
    }
    this.project.images.forEach((image) => {
      this.storage.storage.refFromURL(image.url).delete();
    });
    if (this.project.previewImage) this.storage.storage.refFromURL(this.project.previewImage.url).delete();
    this.firebaseService.updateProfile(this.profile);
    this.ref.close();
  }

  removeTechnology(tech: string) {
    for (let i = 0; i < this.tempTechnologies.length; i++) {
      if (tech === this.tempTechnologies[i]) {
        this.tempTechnologies.splice(i, 1);
        break;
      }
    }
  }

  addTechnology(inputTech?: string) {
    let value;
    if (inputTech) value = inputTech;
    else value = this.techCtrl.value;

    if (value) {
      let alreadyExists: boolean = false;
      for (let tech of this.tempTechnologies) {
        if (tech === value) {
          alreadyExists = true;
          window.alert('Technology already added!');
          break;
        }
      }
      if (!alreadyExists) {
        this.tempTechnologies.push(value);
      }
      this.previewTechs = [];
      this.techCtrl.reset();
    } else window.alert('Input necessary!');
  }

  replLoad() {
    if (this.iframeStartedLoading) this.iframeLoaded = true;

    if (!this.iframeStartedLoading) this.iframeStartedLoading = true;
  }

  openPreviewFile() {
    //open file dialog for preview image
    this.fileInputPreview.nativeElement.click();
  }

  removePreviewImage() {
    this.project.previewImage = null;
    this.project.previewColor = '#222b45';
    this.firstForm.controls.previewColor.setValue('2');
    this.firebaseService.updateProfile(this.profile); //delete preview image from project
    this.storage.storage.refFromURL(this.previewImage.url).delete(); //delete preview image from storage
    this.previewImage = null;
  }

  onPreviewInput(event, user: IUser) {
    //user selected file for preview image
    if (event.target.files.length > 0) {
      let filesplit = event.target.files[0].name.split('.');
      let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
      //only allow img formats
      if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
        let imgSize: number = event.target.files[0].size;
        //1mb max file size
        if (imgSize <= maxImgSize) {
          this.pendingFiles++;
          if (this.previewImage) {
            this.project.previewImage = null; //remove old preview image
            this.firebaseService.updateProfile(this.profile);
            this.storage.storage.refFromURL(this.previewImage.url).delete(); //delete previouse preview image from storage
            this.previewImage = null;
          }
          const file = event.target.files[0];
          const path = 'users/' + user.uid + '/' + Date.now() + '_' + file.name;

          const fileRef = this.storage.ref(path);
          const task = this.storage.upload(path, file);
          this.percentagePreviewImage = task.percentageChanges();

          task
            .snapshotChanges()
            .pipe(
              finalize(() => {
                fileRef.getDownloadURL().subscribe((res) => {
                  this.pendingFiles--;
                  this.previewImage = {
                    name: path,
                    description: '',
                    url: res,
                  };
                  this.project.previewImage = this.previewImage;
                  this.firebaseService.updateProfile(this.profile);
                  this.firstForm.controls.previewColor.setValue('1');
                });
              })
            )
            .subscribe();
        } else
          window.alert('Max file size currently is 1MB (automatic image compression will be added in a future update)');
      } else window.alert('Only images (jpg, png, gif) allowed!');
    }
  }

  openImageDialog(image: Image) {
    this.dialogService.open(ImageDialogComponent, {
      closeOnBackdropClick: true,
      context: { image: image },
    });
  }
}
