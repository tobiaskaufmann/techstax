import { IUser } from './../../_interfaces/user';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FirebaseService } from './../../_services/firebase.service';
import { NbDialogRef } from '@nebular/theme';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Profile } from 'src/app/_interfaces/profile';
import { Project } from 'src/app/_interfaces/project';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Image } from './../../_interfaces/image';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize, map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { icons } from '../../_interfaces/devicon';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';

const maxImgSize: number = 1100000;

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
})
export class AddDialogComponent implements OnInit {
  @ViewChild('fileInputDropzone') fileInputDropzone: ElementRef;
  @ViewChild('fileInputPreview') fileInputPreview: ElementRef;
  @ViewChild('autoCompleteInput', { read: MatAutocompleteTrigger }) autoComplete: MatAutocompleteTrigger; //used for scroll event to close it

  @Input() profile: Profile;
  @Input() user: IUser;

  firstForm: FormGroup;
  previewImage: Image;
  images: Image[] = []; //already uploaded images (not added to project yet)
  files: File[] = [];
  childFile: any;
  pendingFiles: number = 0; //files that have been added but did not finish uploading yet
  percentagePreviewImage: Observable<number>;
  techs: string[] = [];
  markdownPlaceholder: string = `# h1 Heading
###### h6 Heading

* List item1
* List item2

\`\`\`javascript
var x = "code highlighting";
\`\`\`
  `;

  techCtrl = new FormControl();
  filteredTechs: Observable<string[]>;
  previewTechs: string[] = [];

  constructor(
    protected ref: NbDialogRef<AddDialogComponent>,
    private firebaseService: FirebaseService,
    private fb: FormBuilder,
    private storage: AngularFireStorage
  ) {
    //if autocomplete input is empty, don't display techs
    this.techCtrl.valueChanges.subscribe((tech) => {
      if (this.techCtrl.value && this.techCtrl.value.length > 0) {
        this.setIconArray();
      } else {
        this.previewTechs = [];
      }
    });
    //filter techs on change
    this.filteredTechs = this.techCtrl.valueChanges.pipe(
      map((tech) => (tech ? this._filterTechs(tech) : this.previewTechs.slice()))
    );
  }

  ngOnInit() {
    window.addEventListener('scroll', this.scrollEvent, true);

    this.ref.onBackdropClick.subscribe(() => {
      this.closeDialog();
    });

    this.firstForm = this.fb.group({
      name: ['', [Validators.required]],
      company: [],
      summary: ['', [Validators.required]],
      overview: [],
      description: [],
      challenges: [],
      requirements: [],
      publicUrl: [],
      githubUrl: [],
      previewColor: [], //1 = white, 2 = black (if null set to black)
      repl: [],
    });
  }

  private _filterTechs(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.previewTechs.filter((tech) => tech.toLowerCase().indexOf(filterValue) === 0);
  }
  setIconArray() {
    this.previewTechs = [];
    for (let key in icons) {
      this.previewTechs.push(icons[key].displayName);
    }
  }
  getIcon(inputName: string): string {
    if (icons[inputName.toLowerCase()]) return icons[inputName.toLowerCase()].iconName;
  }

  //if user scrolls inside the dialog, the autocomplete field will close
  scrollEvent = (event: any): void => {
    if (this.autoComplete.panelOpen) this.autoComplete.closePanel();
  };

  closeDialog() {
    this.images.forEach((image) => {
      this.storage.storage.refFromURL(image.url).delete(); //delete uploaded project images
    });
    if (this.previewImage) this.storage.storage.refFromURL(this.previewImage.url).delete(); //delete preview image

    if (this.pendingFiles === 0) this.ref.close();
  }

  addProject() {
    if (this.firstForm.valid && this.techs.length > 0 && this.pendingFiles === 0) {
      let project: Project = {
        name: this.firstForm.value.name,
        technologies: this.techs,
        summary: this.firstForm.value.summary,
        overview: this.firstForm.value.overview,
        company: this.firstForm.value.company,
        publicUrl: this.firstForm.value.publicUrl,
        githubRepository: this.firstForm.value.githubUrl,
        repl: null,
      };

      if (this.firstForm.value.repl) {
        if (this.firstForm.value.repl.substring(0, 15) === 'https://repl.it')
          project.repl = this.firstForm.value.repl += '?lite=true';
      }

      project.images = [];
      if (this.images.length > 0) project.images = this.images;
      if (this.previewImage) project.previewImage = this.previewImage;

      //if no preview image (select disabled) set to dark color; if preview image is set but select is unchanged, set to white
      if (!this.firstForm.value.previewColor && !this.previewImage) project.previewColor = '#222b45';
      else if (!this.firstForm.value.previewColor && this.previewImage) project.previewColor = 'white';
      else if (this.firstForm.value.previewColor == '1') project.previewColor = 'white';
      else project.previewColor = '#222b45';

      if (!this.profile.projects) this.profile.projects = [];
      this.profile.projects.push(project);

      this.firebaseService.updateProfile(this.profile);
      this.ref.close();
    } else window.alert('Please fill out all necessary input forms and wait for images to be uploaded!');
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      if (this.files.length < 6) {
        let filesplit = files.item(i).name.split('.');
        let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
        if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
          let imgSize: number = files.item(i).size;
          if (imgSize <= maxImgSize) {
            this.files.push(files.item(i));
            this.pendingFiles++;
          } else
            window.alert(
              'Max file size currently is 1MB (automatic image compression will be added in a future update)'
            );
        } else window.alert('Only images (jpg, png, gif) allowed!');
      } else window.alert('Only 6 images allowed!');
    }
  }

  addDropzone() {
    this.fileInputDropzone.nativeElement.click();
  }

  onDropClick(event) {
    if (event.target.files.length > 0) {
      if (this.files.length < 6) {
        let filesplit = event.target.files[0].name.split('.');
        let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
        if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
          let imgSize: number = event.target.files[0].size;
          if (imgSize <= maxImgSize) {
            this.files.push(event.target.files[0]);
            this.pendingFiles++;
          } else
            window.alert(
              'Max file size currently is 1MB (automatic image compression will be added in a future update)'
            );
        } else window.alert('Only images (jpg, png, gif) allowed!');
      } else window.alert('Only 6 images allowed!');
    }
  }

  receiveFileToDelete(file: File) {
    for (let index = 0; index < this.files.length; index++) {
      if (file.name == this.files[index].name) {
        this.files.splice(index, 1); //remove from client view
        break;
      }
    }
    for (let index = 0; index < this.images.length; index++) {
      if (file.name == this.images[index].name) {
        this.images.splice(index, 1); //remove from images array
        break;
      }
    }
  }

  newImageUploaded($event: Image) {
    this.pendingFiles--;
    this.images.push($event);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.techs, event.previousIndex, event.currentIndex);
  }

  addTechnology(inputTech?: string) {
    let value;
    if (inputTech) value = inputTech;
    else value = this.techCtrl.value;

    if (value) {
      let alreadyExists: boolean = false;
      for (let tech of this.techs) {
        if (tech === value) {
          alreadyExists = true;
          window.alert('Technology already added!');
          break;
        }
      }
      if (!alreadyExists) {
        this.techs.push(value);
      }
      this.previewTechs = [];
      this.techCtrl.reset();
    } else window.alert('Input necessary!');
  }

  removeTechnology(tech: string) {
    for (let i = 0; i < this.techs.length; i++) {
      const el = this.techs[i];
      if (el === tech) {
        this.techs.splice(i, 1);
        break;
      }
    }
  }

  removePreviewImage() {
    this.storage.storage.refFromURL(this.previewImage.url).delete(); //delete preview image
    this.firstForm.controls.previewColor.setValue('2');
    this.previewImage = null;
  }

  openPreviewFile() {
    this.fileInputPreview.nativeElement.click();
  }

  onPreviewInput(event) {
    if (event.target.files.length > 0) {
      let filesplit = event.target.files[0].name.split('.');
      let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
      //only allow img formats
      if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
        let imgSize: number = event.target.files[0].size;
        //1mb max file size
        if (imgSize <= maxImgSize) {
          this.pendingFiles++;
          if (this.previewImage) {
            this.storage.storage.refFromURL(this.previewImage.url).delete(); //delete previouse preview image first
            this.previewImage = null;
          }
          const file = event.target.files[0];
          const path = 'users/' + this.user.uid + '/' + Date.now() + '_' + file.name;

          const fileRef = this.storage.ref(path);
          const task = this.storage.upload(path, file);
          this.percentagePreviewImage = task.percentageChanges();

          task
            .snapshotChanges()
            .pipe(
              finalize(() => {
                fileRef.getDownloadURL().subscribe((res) => {
                  this.pendingFiles--;
                  this.previewImage = {
                    name: path,
                    description: '',
                    url: res,
                  };
                  this.firstForm.controls.previewColor.setValue('1');
                });
              })
            )
            .subscribe();
        } else
          window.alert('Max file size currently is 1MB (automatic image compression will be added in a future update)');
      } else window.alert('Only images (jpg, png, gif) allowed!');
    }
  }
}
