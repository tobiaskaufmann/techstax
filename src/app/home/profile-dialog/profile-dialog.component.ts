import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from '../../_services/firebase.service';
import { AuthService } from '../../_services/auth.service';
import { IUser } from '../../_interfaces/user';
import { Profile } from '../../_interfaces/profile';
import { NbDialogRef } from '@nebular/theme';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-profile-dialog',
  templateUrl: './profile-dialog.component.html',
  styleUrls: ['./profile-dialog.component.scss'],
})
export class ProfileDialogComponent implements OnInit {
  @Input() profile: Profile;
  uniqueNameExists: boolean = false;
  firstForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private firebaseService: FirebaseService,
    public authService: AuthService,
    private router: Router,
    protected ref: NbDialogRef<ProfileDialogComponent>
  ) {}

  ngOnInit() {
    this.firstForm = this.fb.group({
      uniqueName: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9]*$')], this.validate.bind(this)],
      displayName: ['', [Validators.required]],
      country: [],
      city: [],
      isRemote: [],
      currentRole: [],
      github: [],
      twitter: [],
      linkedin: [],
      stackoverflow: [],
      roadmap: [],
    });

    this.firstForm.controls.uniqueName.setValue(this.profile.uniqueName);
    this.firstForm.controls.displayName.setValue(this.profile.displayName);
    this.firstForm.controls.country.setValue(this.profile.country);
    this.firstForm.controls.city.setValue(this.profile.city);
    this.firstForm.controls.isRemote.setValue(this.profile.isRemote);
    this.firstForm.controls.currentRole.setValue(this.profile.currentRole);
    this.firstForm.controls.github.setValue(this.profile.github);
    this.firstForm.controls.twitter.setValue(this.profile.twitter);
    this.firstForm.controls.linkedin.setValue(this.profile.linkedin);
    this.firstForm.controls.stackoverflow.setValue(this.profile.stackoverflow);

    if (this.profile.isRemote) {
      this.firstForm.controls.city.disable();
      this.firstForm.controls.country.disable();
    }
  }

  toggleRemote(event) {
    if (event) {
      this.firstForm.controls.city.disable();
      this.firstForm.controls.country.disable();
    } else {
      this.firstForm.controls.city.enable();
      this.firstForm.controls.country.enable();
    }
  }

  cancel() {
    this.ref.close();
  }

  onSubmit(user: IUser) {
    this.firstForm.markAsDirty();
    if (!this.firstForm.valid) {
      if (!this.firstForm.controls.uniqueName.valid)
        window.alert('Please only use alphanumeric characters for your unique name!');
      else window.alert('Please fill out all required input fields!');
      return;
    }

    this.profile.uniqueName = this.firstForm.value.uniqueName.toLowerCase();
    this.profile.displayName = this.firstForm.value.displayName;
    this.profile.isRemote = this.firstForm.value.isRemote;
    this.profile.userDocId = user.uid;

    if (!this.firstForm.value.isRemote) {
      if (!this.firstForm.value.country || !this.firstForm.value.country) {
        //city or country not set
        this.profile.isRemote = true;
        this.profile.country = null;
        this.profile.city = null;
      } else {
        this.profile.country = this.firstForm.value.country;
        this.profile.city = this.firstForm.value.city;
      }
    } else {
      this.profile.country = null;
      this.profile.city = null;
    }

    if (this.firstForm.value.currentRole) this.profile.currentRole = this.firstForm.value.currentRole;
    else this.profile.currentRole = null;
    if (this.firstForm.value.github) this.profile.github = this.firstForm.value.github;
    else this.profile.github = null;
    if (this.firstForm.value.twitter) this.profile.twitter = this.firstForm.value.twitter;
    else this.profile.twitter = null;
    if (this.firstForm.value.linkedin) this.profile.linkedin = this.firstForm.value.linkedin;
    else this.profile.linkedin = null;
    if (this.firstForm.value.stackoverflow) this.profile.stackoverflow = this.firstForm.value.stackoverflow;
    else this.profile.stackoverflow = null;

    this.firebaseService.updateProfile(this.profile).then(() => {
      this.router.navigate(['/' + this.profile.uniqueName]);
    });

    this.ref.close();
  }

  validate(control: AbstractControl): any {
    //validate unique name (check if it already exists)
    return this.firebaseService.queryProfileByUniqueName(control.value).pipe(
      take(1),
      map((res) => {
        if (
          control.value == 'create' ||
          control.value == 'login' ||
          control.value == '404' ||
          control.value == 'profile' ||
          control.value == 'settings' ||
          control.value == 'about' ||
          control.value == 'home' ||
          control.value == 'feed' ||
          control.value == 'search' ||
          control.value == 'admin' ||
          control.value == 'del34590t'
        ) {
          return { taken: true };
        } else {
          if (!res.length) {
            //no elements found
            this.uniqueNameExists = false;
            return null;
          } else {
            if (res[0].uniqueName === this.profile.uniqueName) {
              this.uniqueNameExists = false;
              return null;
            } else {
              //unique id already exists on other profile
              this.uniqueNameExists = true;
              return {
                taken: true,
              };
            }
          }
        }
      })
    );
  }
}
