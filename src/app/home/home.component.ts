import { NewsletterComponent } from './../_components/newsletter/newsletter.component';
import { WorkDialogComponent } from './work-dialog/work-dialog.component';
import { TechStackDialogComponent } from './tech-stack-dialog/tech-stack-dialog.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { ProfileDialogComponent } from './profile-dialog/profile-dialog.component';
import { ProjectDialogComponent } from './project-dialog/project-dialog.component';
import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { AuthService } from 'src/app/_services/auth.service';
import { FirebaseService } from '../_services/firebase.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService, NbMenuService } from '@nebular/theme';
import { Profile } from '../_interfaces/profile';
import { Project } from '../_interfaces/project';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, take } from 'rxjs/operators';
import { IUser } from '../_interfaces/user';
import { filter, map } from 'rxjs/operators';
import { icons } from '../_interfaces/devicon';

const maxImgSize: number = 1100000;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef; //reference to file input with name fileInput
  @ViewChild('fileInputBackground') fileInputBackground: ElementRef;
  @ViewChild('accordionWork') accordionWork;

  isOwner: boolean = false;
  ownerChecked: boolean = false;
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  showingMoreWork: boolean = false;
  userMenu = [{ title: 'Profile' }, { title: 'Settings' }, { title: 'Log out' }];

  profiles$: Observable<Profile[]>;
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    public authService: AuthService,
    private firebaseServcie: FirebaseService,
    private dialogService: NbDialogService,
    private router: Router,
    private storage: AngularFireStorage,
    private nbMenuService: NbMenuService
  ) {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit() {
    //get profile uniquename from route
    this.profiles$ = this.route.paramMap.pipe(
      switchMap((params) => {
        this.isOwner = false;
        this.ownerChecked = false;
        let parameter = params.get('id');

        //unique name of deleted profiles
        if (parameter == 'del34590t')
          return new Observable<Profile[]>((p) => {
            p.next([]);
          });
        else return this.firebaseServcie.queryProfileByUniqueName(parameter);
      })
    );

    this.nbMenuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title)
      )
      .subscribe((title) => {
        if (title === 'Log out') this.authService.signOut();
        else if (title === 'Settings') {
          this.router.navigateByUrl('settings');
        } else if (title === 'Profile') {
          this.redirectToOwner();
        }
      });

    //find out if user is owner of the profile
    this.profiles$.pipe(takeUntil(this.unsubscribe$)).subscribe((profiles: Profile[]) => {
      this.authService.user$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
        if (user && profiles.length > 0 && profiles[0].userDocId === user.uid) {
          //logged in user owns profile
          this.isOwner = true;
        } else this.isOwner = false;
        this.ownerChecked = true;
      });
    });
  }

  redirectToOwner() {
    this.authService.user$.pipe(take(1)).subscribe((user) => {
      this.firebaseServcie
        .queryProfileByUserUid(user.uid)
        .pipe(take(1))
        .subscribe((profiles) => {
          this.router.navigateByUrl(profiles[0].uniqueName);
        });
    });
  }

  getIcon(inputName: string): string {
    if (icons[inputName.toLowerCase()]) return icons[inputName.toLowerCase()].iconName;
  }

  //prevent dom reload if profile changes
  trackByFn(index, item) {
    return item.id;
  }

  toggleAccordionWork() {
    this.accordionWork.toggle();
    this.showingMoreWork = !this.showingMoreWork;
  }

  openAddDialog(profile: Profile, user: IUser) {
    this.dialogService.open(AddDialogComponent, {
      closeOnBackdropClick: false,
      context: { profile: profile, user: user },
    });
  }

  openProjectDialog(project: Project, profile: Profile) {
    this.dialogService.open(ProjectDialogComponent, {
      closeOnBackdropClick: false,
      context: { project: project, profile: profile, isOwner: this.isOwner },
    });
  }

  openEditProfileDialog(profile: Profile) {
    this.dialogService.open(ProfileDialogComponent, {
      closeOnBackdropClick: true,
      context: { profile: profile },
    });
  }

  openEditTechStackDialog(profile: Profile) {
    this.dialogService.open(TechStackDialogComponent, {
      autoFocus: false,
      closeOnBackdropClick: true,
      context: { profile: profile },
    });
  }

  openEditWorkDialog(profile: Profile) {
    this.dialogService.open(WorkDialogComponent, {
      closeOnBackdropClick: true,
      context: { profile: profile },
    });
  }

  openNewsletterDialog() {
    this.dialogService.open(NewsletterComponent, {
      autoFocus: false,
      closeOnBackdropClick: true,
    });
  }

  changeProfilePicture() {
    this.fileInput.nativeElement.click();
  }

  changeBackgroundPicture() {
    this.fileInputBackground.nativeElement.click();
  }

  resetBackgroundPicture(profile: Profile) {
    profile.profileBackgroundPath =
      'https://firebasestorage.googleapis.com/v0/b/dev-profile-28f95.appspot.com/o/default%2Fbackground_arch.jpg?alt=media&token=a99fe628-1e11-401e-bfa0-ce570c4e1062';
    this.firebaseServcie.updateProfile(profile);
  }

  uploadFile(event, profile: Profile, user: IUser, isBackground?: boolean) {
    //upload profile picture/background picture
    if (isBackground == null) isBackground = false;

    const file = event.target.files[0];
    if (file) {
      let filesplit = file.name.split('.');
      let fileExtension = filesplit[filesplit.length - 1].toLowerCase();
      if (fileExtension == 'jpg' || fileExtension == 'gif' || fileExtension == 'png' || fileExtension == 'jpeg') {
        let imgSize: number = file.size;
        if (imgSize <= maxImgSize) {
          let fileName = user.uid;
          if (isBackground) fileName += '_bg';
          const filePath = '/users/' + user.uid + '/' + fileName;
          const fileRef = this.storage.ref(filePath);
          const task = this.storage.upload(filePath, file);

          // observe percentage changes
          this.uploadPercent = task.percentageChanges();
          // get notified when the download URL is available
          task
            .snapshotChanges()
            .pipe(
              finalize(() => {
                this.downloadURL = fileRef.getDownloadURL();
                this.downloadURL.subscribe((res) => {
                  if (isBackground) profile.profileBackgroundPath = res;
                  else profile.profilePicturePath = res;
                  this.firebaseServcie.updateProfile(profile); //save path of image to profile
                });
              })
            )
            .subscribe();
        } else
          window.alert('Max file size currently is 1MB (automatic image compression will be added in a future update)');
      } else window.alert('Only images (jpg, png, gif) allowed!');
    }
  }

  navigateToLogin() {
    if (this.router.url != '/login')
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.url },
      });
    else this.router.navigate(['login']);
  }
}
