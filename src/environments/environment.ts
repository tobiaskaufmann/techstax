// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC7kGdSt5o4RH6nOItdpqkKWGLNu-yztQY",
    authDomain: "dev-profile-28f95.firebaseapp.com",
    databaseURL: "https://dev-profile-28f95.firebaseio.com",
    projectId: "dev-profile-28f95",
    storageBucket: "gs://dev-profile-28f95.appspot.com",
    messagingSenderId: "116691850732",
    appId: "1:116691850732:web:c3b899705e5bc713a95cc9",
    measurementId: "G-HPFWS15E4Z"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
